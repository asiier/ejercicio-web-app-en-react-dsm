import React from 'react';
import Popover from 'react-bootstrap/Popover';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import { faEthereum } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons';
import { Col, Row, Button, ButtonGroup, Badge } from 'react-bootstrap';
import {BrowserRouter as  Router, NavLink} from 'react-router-dom';

class CartPopUp extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        show: false
      };
    }

    toggle() {
      this.setState(({ show }) => ({ show: !show }));
    }

    exit(e) {
      e.stopPropagation();
      this.setState({show:false})
    }

    render() {
      let list = (<div>No hay NTFs en tu carro 🙁</div>);

      if (Object.keys(this.props.items).length > 0) {
        list = Object.entries(this.props.items).map(item => {
          return(
          <Row key={item[1].name} className="mt-2">
            <Col style={{flex: "0 0 37%", maxWidth: "37%", fontSize: "10px"}}>
              <Row style={{marginLeft: "0px", display: "flex", flexDirection: "row", alignItems: "center", justifyContent: "flex-start", flexWrap: "nowrap"}}>
                <div><img src={item[1].url} style={{maxWidth: "25px"}}></img></div>
                <div style={{marginLeft: "5px"}}>{" " + item[1].name}</div>
              </Row>
            </Col>
            <Col style={{flex: "0 0 35%", maxWidth: "35%"}}>
              <ButtonGroup size="sm">
                <Button onClick={e=>this.props.removeItem(e, item[0])}>-</Button>{' '}
                <Button variant="secondary">{item[1].quantity}</Button>{' '}
                <Button onClick={e=>{this.props.addItem(e ,item[0])}}>+</Button>{' '}
              </ButtonGroup>
            </Col>
            <Col style={{flex: "0 0 20%", maxWidth: "20%"}}>
              <h5>
                <Badge variant="success">{parseFloat(item[1].price * item[1].quantity).toFixed(1) + " "}<FontAwesomeIcon icon={ faEthereum } /></Badge>
              </h5>
            </Col>
          </Row>)
        })
        list = (
          <>
            {list}
            <h3 className="mt-3">Total:  <Badge variant="success">{parseFloat(this.props.total).toFixed(3) +" "}<FontAwesomeIcon icon={ faEthereum } /></Badge></h3>
            <NavLink to="/checkout" onClick={()=>this.setState({show:false})}><Button style={{minWidth: "100%"}}>REALIZAR PEDIDO</Button></NavLink>
          </>
        )
      }

      const popover = (
        <Popover id="popover-basic">
          <Popover.Title >
            <Row>
              <Col md="10">Tus futuros NTFs 🤑🤑</Col>
              <div>
                <Button  style={{marginLeft: "65%"}} onClick={(e)=>this.exit(e)} size="sm">X</Button>
              </div>
            </Row>
          </Popover.Title>
          <Popover.Content>
            {list}
          </Popover.Content>
        </Popover>
      );

      const Default = () => (
          <OverlayTrigger show={this.state.show} placement="bottom" overlay={popover}>
            <FontAwesomeIcon onClick={()=>this.toggle()} icon={ faShoppingCart } style={{ fontSize: "30px", margin:"0 auto"}}/>
          </OverlayTrigger>
      );

      return(
          <>
          <Default></Default>
          </>
      );
    }
}

export default CartPopUp;

