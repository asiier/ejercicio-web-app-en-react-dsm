export class API {
	static url = `https://web-app-dsm-default-rtdb.europe-west1.firebasedatabase.app/users.json`;
	static urlRoot = `https://web-app-dsm-default-rtdb.europe-west1.firebasedatabase.app/users/`;

	static postHeaders = body => {return {method:"POST", headers: {'Content-Type': 'application/json'}, body:JSON.stringify(body)}}
	static deleteHeaders = () => {return {method:'DELETE', headers: {'Content-Type': 'application/json'}}}

	static getUserByName = username =>
		fetch(`${this.url}?orderBy="username"&equalTo="${username}"`)
		.then(response => response.json())

	static isUserSignedIn = () =>
		fetch(this.url)
		.then(response => response.json())

	static isUserSignedIn = async () => await this.getUserByName()

	static getUser = (user) =>
		fetch(this.urlRoot + user + ".json")
		.then(response => response.json())

	static postUser = (user, data) =>
		fetch(this.urlRoot + user + ".json" , this.postHeaders(data))
		.then(response => response.json())

	static postOrder = (user, data) =>
		fetch(this.urlRoot + user + "/orders.json" , this.postHeaders(data))
		.then(response => response.json())

	static deleteOrder = (user, id) => fetch(this.urlRoot + user + "/orders/" + id +".json" , this.deleteHeaders())
}