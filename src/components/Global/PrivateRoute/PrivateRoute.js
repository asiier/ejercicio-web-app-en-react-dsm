import React from 'react';
import {BrowserRouter as Router, Route, NavLink, Redirect} from 'react-router-dom';

class PrivateRoute extends React.Component {
    render() {
        let content = (
            <>
				<Redirect from={this.props.path} to={this.props.to}></Redirect>
            </>
        );

        if (this.props.isLogged) {
           content = (
                <Route path={this.props.path}>
                    {this.props.children}
                </Route>
            );
        }

        return(
            <>
                {content}
            </>
        );
    }
}

export default PrivateRoute;
