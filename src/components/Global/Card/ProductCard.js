import React from 'react';

import { faEthereum } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Card, Row, Col, Button, Badge } from 'react-bootstrap';

class ProductCard extends React.Component {
    render () {
        return (
            <>
            <Card style={{ width: '18rem', fontSize: "15px", marginBottom:"30px"}}>
                {/* <FontAwesomeIcon icon={this.props.product} style={{ fontSize: "60px", margin:"0 auto"}}/> */}
                <img src={this.props.product.url}></img>
                <Card.Body style={{ width: '110%', marginLeft:"-5%"}}>
                    <Card.Title>{this.props.product.name + " - "  }<Badge variant="success">{this.props.product.price + " "}<FontAwesomeIcon icon={ faEthereum } /></Badge>{' '}</Card.Title>
                    <Card.Text style={{whiteSpace: "break-spaces"}}>{"🔰 Collection: " + this.props.product.collection + "🔰\n💠 ID: #" + this.props.product.id + " 💠"}</Card.Text>
                    <Row className="justify-content-md-center">
                        <Col style={{width:"60px"}}><Button onClick={e=>this.props.removeItem(e)}>-</Button></Col>
                        <Col md="auto" style={{textAlign:"center", fontSize:"20px"}}>{this.props.product.quantity}</Col>
                        <Col style={{width:"60px"}}><Button  onClick={e=>this.props.addItem(e)}>+</Button></Col>
                    </Row>
                </Card.Body>
            </Card>
            </>
        );
    };
};

export default ProductCard;


