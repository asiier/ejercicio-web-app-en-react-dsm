import React from 'react';
import ProductCard from '../../Global/Card/ProductCard'
import { Row, Col, Container, Image } from 'react-bootstrap';


class Galery extends React.Component {
    render() {
        return(
            <>
            <Image src="youwouldn't.png" className='fix-image right'></Image>
            <Image rounded src="rightclick.png" className='fix-image left'></Image>
            <Container style={{marginTop: "10%", display: "flex", justifyContent: "center"}}>
                <Row className="justify-content-md-center" style={{maxWidth: "90%"}}>
                    {Object.entries(this.props.products).map(product => (
                        <Col key={product[1].name} xs="auto"><ProductCard removeItem={e=>this.props.removeItem(e, product[0])} addItem={e=>this.props.addItem(e, product[0])} style={{marginBottom:"20px"}} product={product[1]}/></Col>
                        ))}
                </Row>
            </Container>
            </>
        );
    }
}

export default Galery;