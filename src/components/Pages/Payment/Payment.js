import React from 'react';
import { API } from '../../Global/API/API';
import { faEthereum } from '@fortawesome/free-brands-svg-icons';
import { faCreditCard } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { ThemeConsumer } from 'react-bootstrap/esm/ThemeProvider';
import {BrowserRouter as Router, NavLink} from 'react-router-dom';
import { Container, Form, Button, Row, Col, InputGroup } from 'react-bootstrap';


class Payment extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			order:{
				products: [],
				name:"",
				lastname:"",
                username:"",
                email: "",
				password:"",
				metaverse:"",
				key:"",
				saveInfo:"",
				paymentMethod:"",
				pickyPromise:"",
				total:""
            },
			validated: false
		};
	}

	componentDidMount() {
		let order = Object.assign({}, this.state.order);
		order["products"] =  this.props.items;
		this.setState({order:order});
	}

	onChange = (event) => {
		let nam = event.target.name;
		let val = event.target.value;
		let order = Object.assign({}, this.state.order);
		order[nam] = val;
		order.total = parseFloat(this.props.total).toFixed(3);

		let result = true;
		Object.entries(order).forEach(item =>{
			if(item[0] === "products") return;
			if(item[0] === "saveInfo") return;
			if(item[0] === "total") return;

			result = result && (item[1] !== "")
		});

		this.setState({order:order, validated:result});
	}

	proccessOrder() {
		API.getUserByName(this.state.order.username)
		.then( user => {
			API.postOrder(Object.keys(user)[0], this.state.order)
		})
	}

	handleSubmit() {
		if(window.confirm("¿Estas seguro que quieres realizar el pedido?\n¡Los NFTs son un activo especulativo con tendencias burbujisticas, tal vez no sea la merjor inversión!")){
			this.proccessOrder()
		}
	};

	render() {
		let header = (
			<div style={{paddingTop: "0"}} className="py-5 text-center">
				<FontAwesomeIcon icon={faEthereum} className="d-block mx-auto mb-4"  size="6x"/>
				<h2>Pago</h2>
				<p className="lead">Debe rellenar toda la informacion del formulario.</p>
			</div>
		)

		let promo = (
			<li className="list-group-item d-flex justify-content-between bg-light">
						<div className="text-success">
							<h6 className="my-0">Promo code</h6>
							<small>EXAMPLECODE</small>
						</div>
						<span className="text-success">−$5</span>
					</li>
		)

		let cart = (
			<Col md="5" lg="4" className="order-md-last">
				<h4 className="d-flex justify-content-between align-items-center mb-3">
					<span className="text-primary">Tu compra</span>
					<span className="badge bg-primary rounded-pill">{this.props.num}</span>
				</h4>
				<ul className="list-group mb-3">
					{Object.entries(this.props.items).map((item)=>(
						<li key={item[1].name} className="list-group-item d-flex justify-content-between lh-sm">
							<div>
								<h6 className="my-0">
									<img src={item[1].url} style={{maxWidth: "30px"}}></img>  {item[1].name}
								</h6>
								<small className="text-muted">x Cantidad: {item[1].quantity}</small>
							</div>
							<div>
								<span className="text-muted">{parseFloat(item[1].price).toFixed(3)} <FontAwesomeIcon icon={ faEthereum }/></span>
								<div style={{marginTop: "10%"}}></div>
								<small className="text-muted">{parseFloat(item[1].quantity * item[1].price).toFixed(3)} <FontAwesomeIcon icon={ faEthereum }/></small>
							</div>
						</li>
					))}

					<li className="list-group-item d-flex justify-content-between">
						<strong>Total:</strong>
						<strong>{parseFloat(this.props.total).toFixed(3)} <FontAwesomeIcon icon={ faEthereum }/></strong>
					</li>
				</ul>

				<form className="card p-2">
					<div className="input-group">
						<input type="text" className="form-control" placeholder="Promo code"/>
						<button type="submit" className="btn btn-secondary">Canjear</button>
					</div>
				</form>
			</Col>
		)


		let footer = (
			<footer className="my-5 pt-5 text-muted text-center text-small">
				<p className="mb-1">&copy; 2014-2022 Ozuna Networks, Inc.</p>
				<ul className="list-inline">
					<li className="list-inline-item"><a href="#privacy">Privacy</a></li>
					<li className="list-inline-item"><a href="#terms">Terms</a></li>
					<li className="list-inline-item"><a href="#support">Support</a></li>
				</ul>
			</footer>
		)

		let paymentInfo = (
			<>
				<h4 className="mb-3">Información de pago</h4>
				<Form.Group as={Row} className="g-3">
					<Col style={{marginBottom: "1%"}} sm="6">
						<Form.Label htmlFor="firstName">Nombre</Form.Label>
						<Form.Control onChange={e=>this.onChange(e)} name="name" id="firstName" placeholder="Tu nombre" required/>
						<Form.Control.Feedback>Este campo es obligatorio</Form.Control.Feedback>
					</Col>

					<Col style={{marginBottom: "1%"}} sm="6">
						<Form.Label htmlFor="lastName">Apellidos</Form.Label>
						<Form.Control onChange={e=>this.onChange(e)} name="lastname" type="text" id="lastName" placeholder="Tu apellido" required/>
						<Form.Control.Feedback className="invalid-feedback">Este campo es obligatorio</Form.Control.Feedback>
					</Col>

					<Col style={{marginBottom: "1%"}} sm="12">
						<Form.Label htmlFor="username" className="form-label">Usuario (<NavLink to="/login">Aun no tengo cuenta</NavLink>)</Form.Label>
						<InputGroup className="has-validation">
							<InputGroup.Prepend>
								<InputGroup.Text id="basic-addon1">@</InputGroup.Text>
							</InputGroup.Prepend>
							<Form.Control onChange={e=>this.onChange(e)} name="username" id="username" placeholder="Bored Ape #7686" aria-label="Username" aria-describedby="basic-addon1" required/>
							<Form.Control.Feedback>Inserta tu name de usuario</Form.Control.Feedback>
						</InputGroup>
					</Col>

					<Col style={{marginBottom: "1%"}} xs={12} md={12}>
						<Form.Label htmlFor="email">Email</Form.Label>
						<InputGroup className="has-validation">
							<Form.Control onChange={e=>this.onChange(e)} name="email" id="email" placeholder="tiburon@setupropiojefe.com" type="email" aria-label="email" aria-describedby="basic-addon1" required/>
							<Form.Control.Feedback>Indica un email valido para poder contactar contigo</Form.Control.Feedback>
						</InputGroup>
					</Col>

					<Col style={{marginBottom: "1%"}} xs={12} md={12}>
						<Form.Label htmlFor="password">Contraseña</Form.Label>
						<InputGroup className="has-validation">
							<Form.Control onChange={e=>this.onChange(e)} name="password" id="password" type="password"  placeholder="BoredApeNTF1234" aria-label="password" aria-describedby="basic-addon1" required/>
							<Form.Control.Feedback>Este campo es obligatorio</Form.Control.Feedback>
						</InputGroup>
					</Col>

					<Col style={{marginBottom: "1%"}} md={4}>
						<Form.Label htmlFor="metaverse">Metaverso</Form.Label>
						<Form.Control onChange={e=>this.onChange(e)} name="metaverse" as="select" id="metaverse" required>
							<option value="">Elige...</option>
							<option>Second Life (OG)</option>
							<option>Metaverso by Meta™️</option>
							<option>Sandbox</option>
							<option>Cryptovoxel</option>
							<option>Upland</option>
							<option>Genesis World</option>
							<option>NFT Worlds</option>
							<option>Matrix World</option>
							<option>Start Exploring NFT Games</option>

						</Form.Control>
						<Form.Control.Feedback className="invalid-feedback">Por favor selecciona un metaverso</Form.Control.Feedback>
					</Col>

					<Col md={8}>
						<Form.Label htmlFor="publicKey">Clave pública Metamask (Cartera Etherum)</Form.Label>
							<Form.Control onChange={e=>this.onChange(e)} name="key" id="publicKey" placeholder="0xaD3b4394b3967ce4eb7CA8f11cbB12FdEE3bb7c5" required/>
							<Form.Control.Feedback className="invalid-feedback">La clave pública Metamask es obligatoria</Form.Control.Feedback>
					</Col>
				</Form.Group>

				<hr className="my-4"/>

				<Form.Check onChange={e=>this.onChange(e)} name="saveInfo" type="checkbox"  id="save-info" label="Guarda la informacion para futuras adquisiciones de nuevos NFTs"/>
			</>
		)

		let button = (<Button className="w-100 btn btn-primary btn-lg" onClick={(e)=>{}} type="submit">REALIZAR PEDIDO</Button>);

		if(this.state.validated) {
			button = (<NavLink to="/paymentconfirmation">
						<Button className="w-100 btn btn-primary btn-lg" onClick={(e)=> this.handleSubmit(e)} type="submit">REALIZAR PEDIDO</Button>
					</NavLink>);
		}

		const billingInfo = (
			<>
				<h4 className="mb-3">Dirección de pago</h4>

				<Row className="row gy-3">
					<Col style={{marginRight: "7%"}} className="col-md-7">
						<div key="default-radio"  className="my-3">
							<Form.Check onChange={e=>this.onChange(e)} name="paymentMethod" id="credit" type="radio" label="Transferencia a dirección de Ethereum" required/>
						</div>
						<Form.Label htmlFor="cc-name" className="form-label">Dirección Ethereum de pago</Form.Label >
						<Form.Control onChange={e=>this.onChange(e)} name="nameCompleto" type="text" className="form-control" id="cc-name" placeholder="0xaD3b4394b3967ce4eb7CA8f11cbB12FdEE3bb7c5" disabled={true} required/>
						<Form.Control.Feedback className="invalid-feedback">Este campo es obligatorio</Form.Control.Feedback>
					</Col>
					<img style={{width: "150px", height: "150px"}} src='./ethereumQR.png'></img>
				</Row>
				<hr className="my-2"/>
				<Form.Check onChange={e=>this.onChange(e)} name="pickyPromise" type="checkbox"  id="pickyPromise" label='"Pinky Promise" que he enviado el importe total a la anterior dirección de Ethereum' required/>
				<hr className="my-4"/>
				{button}
			</>
		);

		return(
			<>
			<Container>
				<main>
					{header}
					<div className="row g-5">
						{cart}
						<div className="col-md-7 col-lg-8">
							<Form >
								{paymentInfo}
								<hr className="my-4"/>
								{billingInfo}
							</Form>
						</div>
					</div>
				</main>
				{footer}
			</Container>
			</>
		);
	}
}

export default Payment;




