import React from 'react';
import Particle from './Particle';

import { Button } from 'react-bootstrap';
import {BrowserRouter as Router, NavLink} from 'react-router-dom';
import { faEthereum, faBitcoin, faBtc } from '@fortawesome/free-brands-svg-icons';
import { faMoneyBillWave } from '@fortawesome/free-solid-svg-icons';

class PaymentConfirmation extends React.Component {
    constructor(props) {
		super(props);
		this.state = {
			particles: [],
            lastTime: 0,
            particlePool: [
                faBtc,
                faBitcoin,
                faEthereum,
                faMoneyBillWave,
            ]
		};
	}

    componentDidMount() {
        document.body.style.overflow = "hidden"

        setTimeout(this.calculateDelta.bind(this), 0);
    }

    componentWillUnmount() {
        document.body.style.overflow = "auto"
    }

    calculateDelta() {
        let particles = [...this.state.particles];
        let d = new Date().getMilliseconds();
        let delta = d - this.state.lastTime / 1000 ;

        let num = Math.round((Math.random() * 8) - 6 + 1);
        for (let i = 0; i < num; i++) {
            let dim = Math.round(Math.random()) + 1;
            let particle = {
                pos:{x:  Math.random() * 2000, y:0},
                vel:{x: ((Math.random() * 2) -1) * 0.001, y: -Math.random() * 0.005},
                count: Math.floor(Math.random() * 500),
                icon: this.state.particlePool[Math.round(Math.random() * (this.state.particlePool.length - 1))],
                dim: dim,
                blur: 1 / dim,
                key: Math.random() * 500 + Math.random() * 5000
            }
            particles.push(particle);
        }


        particles = particles.filter(particle => {
            return particle.count > 0
        })

        particles = particles.map(particle => {
            let x = particle.pos.x + delta * particle.vel.x;
            let y =  particle.pos.y - delta * particle.vel.y

            particle.pos = {x: x, y: y}
            particle.count--;
            return particle;
        })

        this.setState({particles:particles, lastTime: d});
        setTimeout(this.calculateDelta.bind(this));
    }

    spawnParticles() {
        return this.state.particles.map( item =>
            <Particle key={item.key} data={item} removeItem={e=>this.remove(e)}></Particle>
        )
    }

    render() {
        return(
            <div>
                {this.spawnParticles()}
                <div className="jumbotron blur">
                    <div className="container">
                        <h1 className="display-3">Gracias por su adquisición</h1>
                        <h5 style={{paddingTop: "25px"}}>Con un poco de suerte todos los NFTs que ha comprado no se han devaluado desde que los ha comprado hasta que los reciba. Si quiere seguir expandiendo su colección todavía quedan mucho NFTs que comprar en la tienda.</h5>
                        <div style={{paddingTop: "50px"}}><NavLink to="/shop"><Button className="btn-lg">Shop</Button></NavLink></div>
                    </div>
                </div>
            </div>
        );
    }
}

export default PaymentConfirmation;