import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';


class Particle extends React.Component {
    constructor(props) {
		super(props);
        this.state = {
            data: this.props.data,
        }
	}

    componentWillUnmount(){}

    render() {
        let style = {
            top:this.state.data.pos.y+"px",
            left: this.state.data.pos.x+"px",
            filter:"blur(" + this.state.data.blur +"px)"
        }

        return(
            <div className="position-absolute" style={style}  >
                <FontAwesomeIcon icon={this.state.data.icon} size={this.state.data.dim + "x"}></FontAwesomeIcon>
            </div>
        );
    }
}

export default Particle;