import React from 'react';
import { API } from '../../Global/API/API';
import { faEthereum } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Container, Form, Button, Row, Col, ListGroup, Accordion, Card, ListGroupItem, Badge } from 'react-bootstrap';

class Orders extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user: {}
        };
    }

    componentDidMount() {
        this.fetchOrders();
    }

    fetchOrders() {
        API.getUser(this.props.user)
        .then(user => this.setState({user: user}))
    }

    deleteOrders(id) {
        if(window.confirm("¿Estas seguro que quieres borrar el pedido?\n¡Esto no es reversible!")){
            API.deleteOrder(this.props.user, id).then(item =>
                this.fetchOrders()
                )
        }
    }

    render() {
        const info = (
            <>
                <Col md="5" lg="4" className="order-md-last">
				<h4 className="d-flex justify-content-between align-items-center mb-3">
					<span className="text-primary">Información Personal</span>
					<span className="badge bg-primary rounded-pill">{this.props.num}</span>
				</h4>
				<ListGroup variant="flush" className="transparent">
                    <ListGroup.Item>
                        <Row><Col>Usuario:</Col><Col>{this.state.user.username}</Col></Row>
                    </ListGroup.Item>
                    <ListGroup.Item>
                        <Row><Col>Email:</Col><Col>{this.state.user.email}</Col></Row>
                    </ListGroup.Item>
                </ListGroup>
			</Col>
            </>
        )

        let orders = (
            <>
                <Card>
                    <Card.Header>
                        Aun no tienes ningun pedido a tu nombre
                    </Card.Header>
                </Card>
            </>
        );

        if(Object.keys(this.state.user).length > 0) {
            if( this.state.user.orders != null) {
                orders = (
                    <Accordion defaultActiveKey="0">
                        {Object.entries(this.state.user.orders).map( (order, index)=>
                            {
                                let products = Object.values(order[1].products).map( product =>
                                    <div>
                                        <img src={product.url} style={{maxWidth: "30px"}}></img>
                                        <div style={{fontSize: "18px",marginLeft: "10px", fontWeight: "bold"}}>{" 💠" + product.name + " x" + product.quantity}</div>
                                    </div>

                                )

                                return (
                                    <Card>
                                        <Card.Header style={{width:"500px"}}>
                                            <Accordion.Toggle as={Button} variant="link" eventKey={index + 1}>
                                                <Row>
                                                    <Col style={{alignSelf: "center"}}>
                                                        Pedido #{index +1}
                                                    </Col>
                                                    <Col>
                                                        <Button variant="danger" onClick={()=>this.deleteOrders(order[0])}>Borrar</Button>
                                                    </Col>
                                                </Row>
                                            </Accordion.Toggle>
                                            </Card.Header>
                                            <Accordion.Collapse eventKey={index + 1}>
                                            <Card.Body>
                                                <Row>
                                                    <Col>
                                                        <ListGroup variant="flush" className="transparent">
                                                            <ListGroup.Item>
                                                                <Row><Col>Metaverso:</Col><Col>{order[1].metaverse}</Col></Row>
                                                            </ListGroup.Item>

                                                            <ListGroup.Item>
                                                                <Row><Col >Dirección Ethereum:</Col><Col style={{ whiteSpace: "nowrap", overflow: "hidden", textOverflow: "ellipsis" }}>{order[1].key}</Col></Row>
                                                            </ListGroup.Item>

                                                            <ListGroup.Item>
                                                                <Row><Col style={{ fontSize: "20px", fontWeight: "bold"}}>Total:</Col><Col style={{ fontSize: "20px", fontWeight: "bold"}}>{parseFloat(order[1].total).toFixed(3)} <FontAwesomeIcon icon={ faEthereum }/></Col></Row>
                                                            </ListGroup.Item>
                                                        </ListGroup>
                                                    </Col>
                                                    <Col>
                                                        {products}
                                                    </Col>
                                                </Row>
                                            </Card.Body>
                                        </Accordion.Collapse>
                                    </Card>
                                );
                            }
                        )}
                    </Accordion>
                )
            }
        }

        return(
            <>
                <Row>
                    <Col md={8} >
                        {orders}
                    </Col>
                    <Col md={4}>
                        {info}
                    </Col>
                </Row>
            </>
        );
    }
}

export default Orders;