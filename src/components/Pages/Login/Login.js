import React from 'react';
import { API } from '../../Global/API/API';
import { Form, Button, Alert } from 'react-bootstrap';
import { faUser } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { BrowserRouter as Router, Link, Redirect } from 'react-router-dom';

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user:{
                email: "",
                username:"",
                password: "",
            },
            show: false,
            redirect: false,
            showNewAccount: false
        };
    }

    submit(e) {
        e.preventDefault();
        this.isUserSignedIn();
    }

    isUserSignedIn() {
        API.getUserByName(this.state.user.username)
        .then(response => {
            if(Object.keys(response).length === 0) this.signIn();
            else this.login(response);
        });
    }

    signIn () {
        API.postUser(this.state.user).then(()=>{
            API.getUser(this.state.user.username).then(response => {
                this.logged(Object.keys(response)[0]);
            });
        })
    }

    showAlert(error) {
        this.setState({show: true, error:error});
        setTimeout(()=>{ this.setState({show: false}); }, 5000);
    }

    showNewAccount() {
        this.setState({showNewAccount: true});
        setTimeout(()=>{ this.setState({showNewAccount: false}); }, 5000);
    }

    login(response) {
        let user = Object.values(response)[0];
        let error = null;

        if ( user.username !== this.state.user.username) error = "El nombre de usuario";
        if ( user.email !== this.state.user.email) error = "El email";
        if ( user.password !== this.state.user.password) error = "La contraseña";

        if (error) {
            error = error + ' introducido no coincide\nPorfavor compruebelo y pruebe otra vez :)'
            this.showAlert(error);
            return;
        }

        this.logged(Object.keys(response)[0]);
    }

    logged(userid) {
        this.props.onUserLogged(userid);
        this.setState({
            redirect: true
        })
    }

    onChange = (event) => {
        let nam = event.target.name;
        let val = event.target.value;
        let user = Object.assign({}, this.state.user);
        user[nam] = val;
        this.setState({user:user});
    }

    render() {
        let alert = (
            <>
                <Alert className="fixed-top" show={this.state.show} variant="danger">
                    <Alert.Heading>{this.state.error}</Alert.Heading>
                </Alert>
            </>
        )

        return(
            <>
                { this.state.redirect ? (<Redirect push to="/"/>) : null }
                <Form onSubmit={(e)=>this.submit(e)}>
                    <FontAwesomeIcon icon={faUser} size="6x" style={{textAlign:"center", width:"100%"}}className="mb-4" />
                    <h1 className="h3 mb-3 fw-normal">Inicio de Sesión</h1>

                    <Form.Group>
                        <Form.Label htmlFor="usernameInput">Nombre de usario</Form.Label>
                        <Form.Control onChange={e=>this.onChange(e)} type="text"  name='username' id="usernameInput" placeholder="Bored Ape #7686"/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label htmlFor="emailInput">Email</Form.Label>
                        <Form.Control onChange={e=>this.onChange(e)} type="email"  name='email' id="emailInput" placeholder="tiburon@setupropiojefe.com"/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label htmlFor="passwordInput">Contraseña</Form.Label>
                        <Form.Control onChange={e=>this.onChange(e)} type="password"  name='password' id="passwordInput" placeholder="BoredApeNTF1234"/>
                    </Form.Group>

                    <Form.Group>
                        <Form.Check type="checkbox" value="remember-me" label="Recuerdame"/>
                    </Form.Group>

                    <Button className="w-100 btn btn-lg btn-primary" type="submit">INICIAR SESIÓN</Button>
                </Form>
                    <p className="mt-5 mb-3 text-muted">&copy; 2014-2022 Ozuna Networks, Inc.</p>
                    <ul className="list-inline text">
                        <li className="list-inline-item"><a href="#privacy">Privacy</a></li>
                        <li className="list-inline-item"><a href="#terms">Terms</a></li>
                        <li className="list-inline-item"><a href="#support">Support</a></li>
				    </ul>
                {alert}
            </>
        );
    }
}

export default Login;