import React from 'react';
import CartItem from './CartItem';

//import {BrowserRouter as Router, Link} from 'react-router-dom';
import {BrowserRouter as Router, NavLink} from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEthereum } from '@fortawesome/free-brands-svg-icons';
import { ListGroup, Container, Button, Col, Row,Badge} from 'react-bootstrap';

class CheckOut extends React.Component {
    render() {
        const price  = (
            <>
                <ListGroup className="mt-5">
                    <ListGroup.Item>
                        <Row>
                            <Col md="2">
                                <h3>Total:</h3>
                            </Col>
                            <Col md="6">
                                <h3><Badge variant="success">{parseFloat(this.props.total).toFixed(3) + " "}<FontAwesomeIcon icon={ faEthereum } /></Badge></h3>
                            </Col>
                            <Col md="4">
                                <NavLink to="/payment" onClick={()=>this.setState({show:false})}><Button>CONFIRMAR PEDIDO</Button></NavLink>
                            </Col>
                        </Row>
                    </ListGroup.Item>
                </ListGroup>
            </>
        )

        let body = (
            <div>
                <h2 style={{marginTop:"200px"}}>Todavia no has añadido ningun NTF a tu carrito 😅</h2>
                <h3>Prueba a ir a la:<NavLink to="/shop" className="nav-link active" style={{fontWeight: "bold", color: "#5e90ea"}}>TIENDA</NavLink></h3>
            </div>
        );

        if (Object.keys(this.props.items).length > 0) {
            body =(
                <>
                <ListGroup className="mt-5">
                    <ListGroup.Item>
                        <h3>CONFIRMACION DEL PEDIDO</h3>
                    </ListGroup.Item>
                </ListGroup>

                <ListGroup className="mt-5">
                    {Object.entries(this.props.items).map(item => (
                        <ListGroup.Item key={item[1].name}>
                            <CartItem removeItem={e=>this.props.removeItem(e, item[0])} addItem={e=>this.props.addItem(e, item[0])} style={{marginBottom:"20px"}} item={item[1]}/>
                        </ListGroup.Item>
                    ))}
                </ListGroup>
                {price}
                </>
            )
        }

        return(
            <>
            <Container style={{width: "42rem"}}>
                {body}
            </Container>
            </>
        );
    }
}

export default CheckOut;