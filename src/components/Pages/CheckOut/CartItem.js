import React from 'react';
import { faEthereum } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Col, Row, Button, ButtonGroup, Badge } from 'react-bootstrap';

class CartItem extends React.Component {
    render() {
        return(
            <>
              <Row className="mt-2">
                <Col md="5">
                  <Row style={{marginLeft: "0px", display: "flex", flexDirection: "row", alignItems: "center", justifyContent: "flex-start", flexWrap: "nowrap"}}>
                    <div><img src={this.props.item.url} style={{maxWidth: "30px"}}></img></div>
                    <div style={{fontSize: "18px",marginLeft: "10px"}}>{" 💠" + this.props.item.name}</div>
                  </Row>
                </Col>
                <Col>
                  <ButtonGroup size="sm">
                    <Button onClick={e=>this.props.removeItem(e)}>-</Button>{' '}
                    <Button variant="secondary">{this.props.item.quantity}</Button>{' '}
                    <Button onClick={e=>{this.props.addItem(e)}}>+</Button>{' '}
                  </ButtonGroup>
                </Col>
                <Col>
                  <h5>
                    <Badge variant="success">{parseFloat(this.props.item.price * this.props.item.quantity).toFixed(3) + " "}<FontAwesomeIcon icon={ faEthereum } /></Badge>
                  </h5>
                </Col>
            </Row>
            </>
        );
    }
}

export default CartItem;