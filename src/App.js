// Libs
import './App.css';
import React from 'react';
import { Col, Row, Image } from 'react-bootstrap';
import {BrowserRouter as Router, Switch, Route, NavLink, Redirect} from 'react-router-dom';
// Pages
import Login from './components/Pages/Login/Login';
import Orders from './components/Pages/Orders/Orders';
import Galery from './components/Pages/Galery/Galery';
import Payment from './components/Pages/Payment/Payment';
import CheckOut from './components/Pages/CheckOut/CheckOut';
import CartPopUp from './components/Global/CartPopUp/CartPopUp';
import PrivateRoute from './components/Global/PrivateRoute/PrivateRoute';
import PaymentConfirmation from './components/Pages/PaymentConfirmation/PaymentConfirmation';
//Icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUserCircle } from '@fortawesome/free-regular-svg-icons'

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            cart: {},
            products: {},
            isLogged: false
        };
    }

    componentDidMount() {
        //fetch("https://proyecto-49a43-default-rtdb.firebaseio.com/products.json")
        //fetch("./selected.json")

        fetch("./products.json")
        .then(response => response.json())
        .then(jsonData => {
            Object.keys(jsonData).forEach(key => {
                jsonData[key].quantity = 0;
                jsonData[key].price = parseFloat(jsonData[key].price * Math.random() * 2).toFixed(3);
            });
            this.setState({products: jsonData})
            console.log(this.state)
        })
        this.setState({cart: {}});
    }

    removeItem(e, key) {
        e.stopPropagation();
        let new_state = Object.assign({}, this.state);
        let curProducts = new_state.products;
        if(curProducts[key].quantity <= 0) return;
        curProducts[key].quantity -= 1;
        this.setState({products: curProducts});
        this.cart();
    }

    addItem(e, key) {
        e.stopPropagation();
        let curState = Object.assign({}, this.state);
        let curProducts = curState.products;
        curProducts[key].quantity += 1;
        this.setState({products: curProducts});
        this.cart();
    }

    cart() {
        let valid = {};
        let curState = Object.assign({}, this.state);
        let products = curState.products;
        let total = 0;	//precio total
        let num = 0; //numero total de objetos.

        console.log(curState);

        Object.keys(products).forEach(key => {
            if(products[key].quantity <= 0) return;
            num += products[key].quantity;
            valid[key] = products[key];
            total += products[key].quantity * products[key].price;
            console.log(products[key].quantity, products[key].price, products[key]);
        });

        this.setState({cart: valid, total:total, num: num});
    }

    login(user) {
        console.log(user);
        this.setState({user: user, isLogged:true });
    }

    render() {
        return (
            <div className="App">
                <Router>
                    <div>
                        <header className="masthead fixed-top" style={{height:"72px", backgroundColor:"#04111c", display: "flex", alignContent: "center", alignItems: "center"}}>
                            <Row className='container-fluid' style={{alignItems: "center"}}>
                                <Image src="/logo512.png" style={{width: "85px"}}></Image>
                                <Col className='center'>
                                    <h3 className="masthead-brand" style={{fontFamily: "Poppins, sans-serif", fontWeight: "bold"}}>MarAbierto</h3>
                                </Col>
                                <Col>
                                    <nav className="nav nav-masthead justify-content-center">
                                        <NavLink style={{marginLeft: "2rem"}} to="/shop"  activeClassName="active" className="nav-link">Shop</NavLink>
                                        <NavLink style={{marginLeft: "2rem"}} to="/" className="nav-link"><CartPopUp removeItem={(e,i)=>this.removeItem(e,i)} addItem={(e,i)=>this.addItem(e,i)} items={this.state.cart} total={this.state.total}></CartPopUp></NavLink>
                                        <NavLink style={{marginLeft: "2rem"}} to="/users" activeClassName="active" className="nav-link" >
                                            <FontAwesomeIcon icon={faUserCircle} style={{ fontSize: "30px", margin:"0 auto"}}></FontAwesomeIcon>
                                        </NavLink>
                                    </nav>
                                </Col>
                            </Row>
                        </header>
                    </div>
                    <div style={{marginTop:"100px"}}>
                        <Switch >
                            <Route path="/login">
                                <Login onUserLogged={user=>this.login(user)}/>
                            </Route>
                            <Route path="/checkout">
                                <CheckOut items={this.state.cart} removeItem={(e,i)=>this.removeItem(e,i)} addItem={(e,i)=>this.addItem(e,i)} total={this.state.total} />
                            </Route>
                            <Route path="/payment">
                                <Payment items={this.state.cart} num={this.state.num}  total={this.state.total}/>
                            </Route>
                            <Route path="/paymentconfirmation" to="/">
                                <PaymentConfirmation/>
                            </Route>
                            <PrivateRoute path="/users" to="/login" isLogged={this.state.isLogged}>
                                <Orders user={this.state.user}/>
                            </PrivateRoute>
                            <Route path="/shop">
                                <Galery products={this.state.products} removeItem={(e,i)=>this.removeItem(e,i)} addItem={(e,i)=>this.addItem(e,i)} />
                            </Route>
                            <Redirect from="/" to="/shop"></Redirect>
                        </Switch>
                    </div>
                </Router>
            </div>
        );
    }
}

export default App;
